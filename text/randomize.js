// this fills the variable data_series with random integer values between min and max
function randomize(min, max) {
	for (var i = 0; i < data_series.length; i++) {
    	data_series[i] = Math.random() * (max - min) + min;
	}
};

randomize(-1000,1000);

